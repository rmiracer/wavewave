C       N = # OF NONLINEAR-EQUATIONS., NN # TOTAL OF EQUATIONS
        implicit none
        integer N, NN, tfinal
        PARAMETER(N=4, NN = 20)
        EXTERNAL DERIVS
        double precision Y(NN),step,t,yprime(NN)

        double precision cum(N), znorm(N), gsc(N)
        integer I, J, K, L, IO, NSTEP

        double precision Delta, Gamma2, Gamma3
        double precision dos
        integer mesflg, lunit
        common/cte/ Gamma2, Gamma3, Delta
        common/eh0001/ mesflg, lunit

c variables for lsodar:
      integer itol, itask, istate, iopt, lrw, iwork, liw, jt
      double precision tf,rtol, atol, rwork,jdum
      dimension rwork(22+NN*(NN + 9)),iwork(20+NN)

      mesflg = 0
      OPEN(1, Status='unknown', FILE = 'saida.dat')
      OPEN(2, Status='unknown', FILE = 'lyasaida.dat')

        dos = 2.0

c	Parametros para Russell & Ott
        Delta = -6
        Gamma2 = 6.75


c	Diagrama de bif. mais geral (1/4)
c	do 1001 Gamma3 = 3, 4.75, 0.014
c	Diagrama de bif. mais geral (2/4)
c	do 1001 Gamma3 = 4.75, 6.50, 0.014
c	Diagrama de bif. mais geral (3/4)
c	do 1001 Gamma3 = 6.50, 8.25, 0.014
c	Diagrama de bif. mais geral (4/4)
c	do 1001 Gamma3 = 8.25, 10., 0.014

c	Regiao de interes
c	do 1001 Gamma3 = 7.69, 7.7, .00008
c	do 1001 Gamma3 = 5.775, 5.785, .00008
c        do 1001 Gamma3 = 5.5, 6.5, 0.008

c	Diagrama de bif. geral
c	do 1001 Gamma3 = 6.2, 7., 0.0016

c	Janela P-3
        do 1001 Gamma3 = 6.48, 6.52, .00008


c       initial conditions
        y(1) = 5.26426756
        y(2) = 0.992913321
        y(3) = 1.001
        y(4) = 3.79659238

        write(*,*) 'Gamma3 = ', Gamma3

c initialize parameters for lsodar integrator:
        ITOL=1
        RTOL= 1.0D-10
        ATOL= 1.0D-10
        ITASK=1
        ISTATE=1
        IOPT=1
        JT=2
        LRW = (22+NN*(NN + 9))
        LIW = 20+NN
        JT=2

        iwork(7) = 10



C     INITIAL CONDITIONS FOR LINEAR SYSTEM (ORTHONORMAL FRAME)
C
      DO 10 I=N+1,NN
         Y(I)=0.0
 10      CONTINUE
      DO 20 I=1,N
         Y((N+1)*I)=1.0
         CUM(I)=0.0
         znorm(i) = 0.
 20      CONTINUE

         IO    =   100000
         NSTEP = 1000000

        istate=1

        do i=5,10
           iwork(i) = 0
           rwork(i) = 0.0
        enddo
        iwork(5) = 0

        step=0.01
        T=0.0
        tfinal=1000.

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c loop princial

        k = 0

       do 100 I = 1, NSTEP

        TF  = step * float(I)

c         TF = T + step

        CALL LSODA(DERIVS,NN,Y,T,TF,ITOL,RTOL,ATOL,ITASK,
     *  ISTATE,IOPT,RWORK,LRW,IWORK,LIW,JDUM,JT)

C CONSTRUCT A NEW ORTHONORMAL BASIS BY GRAM-SHIMIDT METHOD
C NORMALIZE FIST VECTOR
         ZNORM(1)=0.0
         DO 30 J = 1,N
            ZNORM(1)=ZNORM(1)+Y(N*J+1)**2
 30         CONTINUE
         ZNORM(1)=SQRT(ZNORM(1))
         DO 40 J=1,N
            Y(N*J+1)=Y(N*J+1)/ZNORM(1)
 40         CONTINUE


C GENERATE  THE NEW ORTHONORMAL SET OF VECTOR

         DO 80 J=2,N

C        GENERATE J-1 GSR COEFFICIENTS.
            DO 50 K = 1,(J-1)
               GSC(K)=0.0
               DO 50 L=1,N
                  GSC(K) = GSC(K) + Y(N*L+J)*Y(N*L+K)
 50         CONTINUE


C        CONTRUCT A NEW VECTOR
            DO 60 K = 1,N
               DO 60 L = 1,(J-1)
                  Y(N*K+J) = Y(N*K+J)-GSC(L)*Y(N*K+L)
 60         CONTINUE

C        CALCULATE THE VECTOR NORM
            ZNORM(J) = 0.0
            DO 70 K = 1,N
               ZNORM(J) = ZNORM(J)+Y(N*K+J)**2
70         CONTINUE

            ZNORM(J) = SQRT(ZNORM(J))


C        NORMALIZE THE NEW VECTOR
            DO 80 K = 1,N
               Y(N*K+J)=Y(N*K+J)/ZNORM(J)
80         CONTINUE

C UPDATE RUNNING VECTOR MAGNITUDES
            DO 90 K = 1,N

c	    ALOG calcula o logaritmo natural para
c	    variavels REAL*4
c	    Nos usamos DLOG para double precision
c              CUM(K)= (CUM(K)+(ALOG(ZNORM(K))/ALOG(2.0)))

               CUM(K)= CUM(K)+DLOG(znorm(k))/DLOG(2.0D0)
c	       write(*, *) k
c	       write(*, *) cum(k)
c	       write(*, *) znorm(k)
c	       write(*, *) dlog(znorm(k))
c	       write(*, *) dlog(2.0D0)
c	       write(*, *)
 90         CONTINUE


C NORMALIZE EXPONENT AND PRINT EVERY IO INTERACTIONS
            IF (MOD(I,IO).EQ.0) THEN
              write(*, *) I, TF, (CUM(K)/TF,K=1,N)
            ENDIF

            istate = 1


100     continue

        write(*, *) 'Final:'
        write(2,161)Gamma3, TF,(CUM(K)/TF,K = 1,N)
        flush(2)
        write(*,161)Gamma3, TF,(CUM(K)/TF,K = 1,N)
        write(*, *)

 161     FORMAT(5(E16.8))

c200    continue

1001    continue

       CLOSE(1)
       CLOSE(2)

       END

c       Bifurcation and "strange" behavior in instability
c       saturation by nonlinear three-wave mode coupling
c       Wersinger et al
c       Phys. Fluids 23(6), 1980

        subroutine derivs_wersinger(n, t, y, yprime)
        implicit none
        integer n
        double precision y(n), yprime(n), t
        double precision Gamma2, Gamma3, Delta
        double precision df1_dy1, df1_dy2, df1_dy3, df1_dy4
        double precision df2_dy1, df2_dy2, df2_dy3, df2_dy4
        double precision df3_dy1, df3_dy2, df3_dy3, df3_dy4
        double precision df4_dy1, df4_dy2, df4_dy3, df4_dy4
        common/cte/ Gamma2, Gamma3, Delta

        yprime(1) = y(1) + y(2)*y(3)*dcos(y(4))

        yprime(2) = -Gamma2*y(2) - y(1)*y(3)*dcos(y(4))

        yprime(3) = -Gamma3*y(3) - y(1)*y(2)*dcos(y(4))

        yprime(4) = -Delta +  ( (y(1)**2)*(y(2)**2)
     *                        + (y(1)**2)*(y(3)**2)
     *                        - (y(2)**2)*(y(3)**2)
     *                        )/( y(1)*y(2)*y(3)) * dsin(y(4))

        df1_dy1 = 1
        df1_dy2 = y(3) * dcos(y(4))
        df1_dy3 = y(2) * dcos(y(4))
        df1_dy4 = -y(2) * y(3) * dsin(y(4))

        df2_dy1 = -y(3) * dcos(y(4))
        df2_dy2 = -Gamma2
        df2_dy3 = -y(1) * dcos(y(4))
        df2_dy4 = y(1) * y(3) * dsin(y(4))

        df3_dy1 = -y(2) * dcos(y(4))
        df3_dy2 = -y(1) * dcos(y(4))
        df3_dy3 = -Gamma3
        df3_dy4 = y(1) * y(2) * dsin(y(4))

        df4_dy1 = y(2)/y(3)*dsin(y(4)) + y(3)/y(2)*dsin(y(4))
     *            + y(2)*y(3)/(y(1)**2) * dsin(y(4))

        df4_dy2 = y(1)/y(3)*dsin(y(4)) - y(3)/y(1)*dsin(y(4))
     *            - y(1)*y(3)/(y(2)**2) * dsin(y(4))

        df4_dy3 = y(1)/y(2)*dsin(y(4)) - y(2)/y(1)*dsin(y(4))
     *            - y(1)*y(2)/(y(3)**2) * dsin(y(4))

        df4_dy4 =   y(1)*y(2)/y(3)*dcos(y(4)) 
     *            + y(1)*y(3)/y(2)*dcos(y(4))
     *            - y(2)*y(3)/y(1)*dcos(y(4))

c       df1/dy(1)
        yprime(5) = df1_dy1*y(5) + df1_dy2*y(9) 
     *             + df1_dy3*y(13) + df1_dy4*y(17)

c       df1/dy(2)
        yprime(6) = df1_dy1*y(6) + df1_dy2*y(10) 
     *             + df1_dy3*y(14) + df1_dy4*y(18)

c       df1/dy(3)
        yprime(7) = df1_dy1*y(7) + df1_dy2*y(11)
     *             + df1_dy3*y(15) + df1_dy4*y(19)

c       df1/dy(4)
        yprime(8) = df1_dy1*y(8) + df1_dy2*y(12)
     *             + df1_dy3*y(16) + df1_dy4*y(20)

c       df2/dy(1)
        yprime(9) = df2_dy1*y(5) + df2_dy2*y(9) 
     *             + df2_dy3*y(13) + df2_dy4*y(17)

c       df2/dy(2)
        yprime(10) = df2_dy1*y(6) + df2_dy2*y(10) 
     *             + df2_dy3*y(14) + df2_dy4*y(18)

c       df2/dy(3)
        yprime(11) = df2_dy1*y(7) + df2_dy2*y(11)
     *             + df2_dy3*y(15) + df2_dy4*y(19)

c       df2/dy(4)
        yprime(12) = df2_dy1*y(8) + df2_dy2*y(12)
     *             + df2_dy3*y(16) + df2_dy4*y(20)

c       df3/dy(1)
        yprime(13) = df3_dy1*y(5) + df3_dy2*y(9) 
     *             + df3_dy3*y(13) + df3_dy4*y(17)

c       df3/dy(2)
        yprime(14) = df3_dy1*y(6) + df3_dy2*y(10) 
     *             + df3_dy3*y(14) + df3_dy4*y(18)

c       df3/dy(3)
        yprime(15) = df3_dy1*y(7) + df3_dy2*y(11)
     *             + df3_dy3*y(15) + df3_dy4*y(19)

c       df3/dy(4)
        yprime(16) = df3_dy1*y(8) + df3_dy2*y(12)
     *             + df3_dy3*y(16) + df3_dy4*y(20)

c       df4/dy(1)
        yprime(17) = df4_dy1*y(5) + df4_dy2*y(9) 
     *             + df4_dy3*y(13) + df4_dy4*y(17)

c       df4/dy(2)
        yprime(18) = df4_dy1*y(6) + df4_dy2*y(10) 
     *             + df4_dy3*y(14) + df4_dy4*y(18)

c       df4/dy(3)
        yprime(19) = df4_dy1*y(7) + df4_dy2*y(11)
     *             + df4_dy3*y(15) + df4_dy4*y(19)

c       df4/dy(4)
        yprime(20) = df4_dy1*y(8) + df4_dy2*y(12)
     *             + df4_dy3*y(16) + df4_dy4*y(20)

        return
        end



c       Chaotic (strange) and periodic behavior in instability
c       saturation by the oscillating two-stream instability
c       David A. Russell, Edward Ott
c       Phys. Fluids 24(11), November 1981

        subroutine derivs(nn, t, y, yprime)
        implicit none
        integer nn, i
        double precision t
        double precision y(nn), yprime(nn)
        double precision Gamma2, Gamma3, Delta
        double precision df1_dy1, df1_dy2, df1_dy3, df1_dy4
        double precision df2_dy1, df2_dy2, df2_dy3, df2_dy4
        double precision df3_dy1, df3_dy2, df3_dy3, df3_dy4
        double precision df4_dy1, df4_dy2, df4_dy3, df4_dy4
        common/cte/ Gamma2, Gamma3, Delta

        yprime(1) = y(1) + 2.*y(1)*(y(2)*y(3))*dsin(y(4))

        yprime(2) = -Gamma2*y(2) - (y(1)*y(1))*y(3)*dsin(y(4))

        yprime(3) = -Gamma3*y(3) - (y(1)*y(1))*y(2)*dsin(y(4))

        yprime(4) = -2.*delta + (y(2)**2 + y(3)**2 - 2*y(1)**2)+
     *          (4.*y(2)*y(3) - y(1)**2 * ( y(3)/y(2) + y(2)/y(3)
     *                                    ))*dcos(y(4))

        df1_dy1 = 1 + 2*y(2)*y(3)*dsin(y(4))
        df1_dy2 = 2*y(1)*y(3)*dsin(y(4))
        df1_dy3 = 2*y(1)*y(2)*dsin(y(4))
        df1_dy4 = 2*y(1)*y(2)*y(3)*dcos(y(4))

        df2_dy1 = -2*y(1)*y(3)*dsin(y(4))
        df2_dy2 = -Gamma2
        df2_dy3 = -y(1)**2 * dsin(y(4))
        df2_dy4 = -y(1)**2 * y(3)*dcos(y(4))
        df3_dy1 = -2*y(1)*y(2)*dsin(y(4))
        df3_dy2 = -y(1)**2 * dsin(y(4))
        df3_dy3 = -Gamma3
        df3_dy4 = -y(1)**2 * y(2)*dcos(y(4))

        df4_dy1 = -4*y(1) - 2*y(1)*(y(3)/y(2) + y(2)/y(3))*dcos(y(4))

        df4_dy2 = 2*y(2) + 4*y(3)*dcos(y(4))
     *                   + y(1)**2 * y(3)*dcos(y(4))/(y(2)**2)
     *                   - y(1)**2 * dcos(y(4))/y(3)

        df4_dy3 = 2*y(3) + 4*y(2)*dcos(y(4)) - y(1)**2 *dcos(y(4))/y(2)
     *                   + y(1)**2 *y(2)*dcos(y(4))/(y(3)**2)

        df4_dy4 = -(4*y(2)*y(3) - y(1)**2 * (y(3)/y(2)
     *                                      + y(2)/y(3)))*dsin(y(4))

c       df1/dy(1)
        yprime(5) = df1_dy1*y(5) + df1_dy2*y(9)
     *             + df1_dy3*y(13) + df1_dy4*y(17)

c       df1/dy(2)
        yprime(6) = df1_dy1*y(6) + df1_dy2*y(10)
     *             + df1_dy3*y(14) + df1_dy4*y(18)

c       df1/dy(3)
        yprime(7) = df1_dy1*y(7) + df1_dy2*y(11)
     *             + df1_dy3*y(15) + df1_dy4*y(19)

c       df1/dy(4)
        yprime(8) = df1_dy1*y(8) + df1_dy2*y(12)
     *             + df1_dy3*y(16) + df1_dy4*y(20)

c       df2/dy(1)
        yprime(9) = df2_dy1*y(5) + df2_dy2*y(9)
     *             + df2_dy3*y(13) + df2_dy4*y(17)

c       df2/dy(2)
        yprime(10) = df2_dy1*y(6) + df2_dy2*y(10)
     *             + df2_dy3*y(14) + df2_dy4*y(18)

c       df2/dy(3)
        yprime(11) = df2_dy1*y(7) + df2_dy2*y(11)
     *             + df2_dy3*y(15) + df2_dy4*y(19)
c       df2/dy(4)
        yprime(12) = df2_dy1*y(8) + df2_dy2*y(12)
     *             + df2_dy3*y(16) + df2_dy4*y(20)
c       df3/dy(1)
        yprime(13) = df3_dy1*y(5) + df3_dy2*y(9)
     *             + df3_dy3*y(13) + df3_dy4*y(17)

c       df3/dy(2)
        yprime(14) = df3_dy1*y(6) + df3_dy2*y(10)
     *             + df3_dy3*y(14) + df3_dy4*y(18)

c       df3/dy(3)
        yprime(15) = df3_dy1*y(7) + df3_dy2*y(11)
     *             + df3_dy3*y(15) + df3_dy4*y(19)

c       df3/dy(4)
        yprime(16) = df3_dy1*y(8) + df3_dy2*y(12)
     *             + df3_dy3*y(16) + df3_dy4*y(20)

c       df4/dy(1)
        yprime(17) = df4_dy1*y(5) + df4_dy2*y(9)
     *             + df4_dy3*y(13) + df4_dy4*y(17)

c       df4/dy(2)
        yprime(18) = df4_dy1*y(6) + df4_dy2*y(10)
     *             + df4_dy3*y(14) + df4_dy4*y(18)

c       df4/dy(3)
        yprime(19) = df4_dy1*y(7) + df4_dy2*y(11)
     *             + df4_dy3*y(15) + df4_dy4*y(19)

c       df4/dy(4)
        yprime(20) = df4_dy1*y(8) + df4_dy2*y(12)
     *             + df4_dy3*y(16) + df4_dy4*y(20)

       return
       end

