        implicit none
        integer nn,ng,transiente,tfinal
        PARAMETER(NN=4, ng=1)
        EXTERNAL DERIVS,g
        double precision Y(NN),step,t,yprime(NN), y_0(NN)

        double precision j,Gamma2, Gamma3, Delta
        integer i,k,iii,jj
        integer mesflg, lunit
        common/cte/ Gamma2, Gamma3, Delta
        common/eh0001/ mesflg, lunit

c variables for lsodar:
      integer itol, itask, istate, iopt, lrw, iwork, liw, jt
      double precision tf,rtol, atol, rwork,jdum
      dimension rwork(22+NN*(16)+3*ng),iwork(20+NN)
      double precision jac
      integer jroot(ng),c

        mesflg = 1
      OPEN(1, Status='unknown', FILE = 'output.dat')

c initialize parameters for lsodar integrator:
        ITOL=1
        RTOL= 1.0D-10
        ATOL= 1.0D-10
        ITASK=1
        ISTATE=1
        IOPT=1
        JT=2
        LRW = (22+NN*16) + 3*ng
        LIW = 20+NN
        JT=2

        iopt=1
        do i=5,10
           iwork(i) = 0
           rwork(i) = 0.0
        enddo
        iwork(5) = 0
        iwork(6) = 10000


c initial conditions 
c        y(1) = 1.33702
c        y(2) = 3.0002
c        y(3) = 0.89091
c        y(4) = 2.79091

        y(1) = 5.26426756
        y(2) = 0.992913321
        y(3) = 1.001
        y(4) = 3.79659238

        y(1) = 4.7204102591468056
        y(2) = 0.99144516484547085
        y(3) = 1.001
        y(4) = 3.8772979343110694

        Delta  = -6.
        Gamma2 = 6.75

        Gamma3 = 6.5150

        write(*,*) 'Gamma3 = ', Gamma3

        istate=1
        
        step=0.01
        t=0.0  
        transiente=1500

c drop transient
        DO 300 k = 1, transiente

           TF = T + 0.0001
           call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1            istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2            g, ng, jroot)
      
           tf = t + 100

           call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1            istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2            g, ng, jroot)
     
c	   istate=2     

 300    continue

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c loop princial 

         k = 0
          
c	 do while (k.lt.300)
         do while (k.lt.1e5)

c um pequeno passo para escapar do plano de poincaré:
         TF = T + 0.0001

         call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1            istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2            g, ng, jroot)

         tf = t + 1000000
c	 istate=2     

         call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1            istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2            g, ng, jroot)
      

         TF = T + 0.0001
c	 istate=2     


         call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1            istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2            g, ng, jroot)

         tf = t + 1000000
c	 istate=2     

         call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1            istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2            g, ng, jroot)
     
c	 istate=2     

         write(1, *) Gamma3, t, y(1), y(2), y(3), y(4)
c	 write(1, *) Gamma3, y(1)

         k = k + 1

         enddo


 161     FORMAT(4(E16.8))

       CLOSE(1)
       
       END


        subroutine derivs(nn,t,y,yprime)
        implicit none
        integer i, nn
        double precision y(nn),yprime(nn), t
        double precision Gamma2, Gamma3, Delta

        common/cte/ Gamma2, Gamma3, Delta

        yprime(1) = y(1) + 2.*y(1)*(y(2)*y(3))*dsin(y(4))

        yprime(2) = -Gamma2*y(2) - (y(1)*y(1))*y(3)*dsin(y(4))

        yprime(3) = -Gamma3*y(3) - (y(1)*y(1))*y(2)*dsin(y(4))

        yprime(4) = -2.*delta + (y(2)**2 + y(3)**2 - 2*y(1)**2)+
     *          (4.*y(2)*y(3) - y(1)**2 * ( y(3)/y(2) + y(2)/y(3)
     *                                    ))*dcos(y(4))

        return
        end


c****************************************************
c  gout=0 determines the Poincare surface of section. 
      subroutine g (n, t, y, ng, gout)
      double precision t,y(n),gout(ng)

          gout(1) = y(3)-1.

      return
      end
      

