cccccccccccccccccc
c  Atencao:
c  Modifique a secao de poincare
cccccccccccccccccc

	IMPLICIT NONE
	integer N, NN, i
	PARAMETER(N=4, NN=20)
	double precision y(NN), y0(N)

	integer mesflg, lunit
	integer flag_lsodar_aborted

	double precision y1_inicial, y2_inicial, y3_inicial
	double precision y4_inicial
	double precision b, c
	double precision autov_real(N), autov_im(N)

	double precision Delta, Gamma2, Gamma3

	external derivs, g, lsodar, dgeev

	integer istate
	double precision t

	double precision k

	double precision vr(N, N)

	common/cte/Gamma2, Gamma3, Delta
	common/eh0001/ mesflg, lunit
	common/newton_upo/ flag_lsodar_aborted

	namelist/init/ Gamma3, y1_inicial, y2_inicial, y3_inicial,
     *                         y4_inicial

	open(2, file = 'saida_upo.dat', status = 'unknown')
	open(1, file = 'init.dat', status = 'old')

	read(1, init)

	mesflg = 1


  	y0(1) = y1_inicial
	y0(2) = y2_inicial
	y0(3) = y3_inicial
	y0(4) = y4_inicial

	Delta = -6
	Gamma2 = 6.75

	do 100 Gamma3 = 6.5074, 6.52, 0.0001

	flag_lsodar_aborted = 0

c  	y0(1) = y1_inicial
c	y0(2) = y2_inicial
c	y0(3) = y3_inicial
c	y0(4) = y4_inicial

	do 200 i = 1, 20

	  if(flag_lsodar_aborted.ne.1) then
	    call find(y0, y, autov_real, autov_im, vr)
	  endif

200	continue


c	write(*, *) 'Para gamma = ', gamma
c	write(*, *) 'Valores iniciais:'
c	write(*, *) y1_inicial, y2_inicial, y3_inicial
c	write(*, *) 'Valor final:'
c	write(*, *) y(1), y(2), y(3)

c	write(*, *) 'Autovalores:'
c	write(*, *) '  Parte real: '
	write(*, *) '  ', autov_real
c	write(*, *) '  Parte img: '
c	write(*, *) '  ', autov_im
c	write(*, *)
c	write(*, *) 'Autovetores:'
c	write(*, *) '  ', vr(1, 1), vr(2, 1), vr(3, 1)
c	write(*, *) '  ', vr(1, 2), vr(2, 2), vr(3, 2)
c	write(*, *) '  ', vr(1, 3), vr(2, 3), vr(3, 3)
c	write(*, *)

	write(2, *) Gamma3, y(1), y(2), y(4)
	write(*, *) Gamma3, y(1), y(2), y(4)

100	continue
	end


	subroutine find(y0, y, autov_real, autov_im, vr)
	implicit none
	integer N, NN
	parameter(N=4, NN=20)
	external derivs, g
	double precision y0(N), y(NN)
	double precision Gamma2, Gamma3, Delta
	double precision J(N, N), B(N, N), dy(N+1)

c	Variavel auxiliar para o calculo da diferenza
c	entre valor anterior e atual
	double precision aux_dif_y(N)

c	Mais variables auxiliares
	double precision b1, b2

	integer i, k

c	Autovalores
	double precision autov_real(N), autov_im(N)

c	Variavels auxiliares para o calculo dos autovalores
	double precision aux1, aux2, aux3

c	Parametros para lsodar
	double precision rtol, atol,
     *  rwork(22+NN*(NN+9)+3), jac
	double precision t, tf
	integer jroot(1), istate, itol, itask, iopt, lrw, iwork(20+NN),
     *    liw, ng, jt

c	Para evitar que o lsodar tire o programa
c	uso uma bandera
	integer flag_lsodar_aborted

c	Parametros para o A x = B solver
c	(en realidade, o sistema ficaria
c	B dy = aux_dif_y, considerando 
c	os nomes das variavels)
	integer lda, ipvt(N), info_solver, job

c	Parametros para o eigenvalue solver
	character jobvl, jobvr
	double precision wr(N), wi(N)
	double precision work(4*N)
	integer info_eigen
c	... ainda nao precisamos dos calculos dos autovetores
	double precision vl(N), vr(N, N)
	integer ldvl, ldvr, lwork
	
	common/cte/Gamma2, Gamma3, delta
	common/newton_upo/ flag_lsodar_aborted

cccccccccccccccccccccccccccccccccccccccccccccccc

	do 300 i = 1, N
	  y(i) = y0(i)
300 	continue

c	Inicializacao da jacobiana de fluxo
        y(5) = 1.
        y(6) = 0.
        y(7) = 0.
        y(8) = 0.

        y(9) = 0.
        y(10) = 1.
        y(11) = 0.
        y(12) = 0.

        y(13) = 0.
        y(14) = 0.
        y(15) = 1.
        y(16) = 0.

        y(17) = 0.
        y(18) = 0.
        y(19) = 0.
        y(20) = 1.

c	Parametros para eigenvalue solver (dgeev)
	jobvl = 'N'
	jobvr = 'V'
	ldvl = 1
	ldvr = N
	lwork = 4 * N

c	Parametros para lsodar
        itol=1
        rtol= 1.0D-10
        atol= 1.0D-10
        itask=1
        istate=1
        iopt=0
        lrw= 22+NN*(NN + 9) + 3
        liw= 20+NN
        jt=2
        iopt=1
	ng = 1
        do i=5,10
           iwork(i) = 0
           rwork(i) = 0.0
        enddo

c	Flag para mostrar informacao
c	cuando lsodar muda de metodo
	iwork(5) = 0

c	Numero maximo de steps durante a integracao
        iwork(6) = 100000

	jroot(1) = 0
	t = 0.0


	do 100 i = 1, 3

c	  Passo para escapar do plano de poincare
    	  tf = t + 0.0001

	  if(flag_lsodar_aborted.eq.1) then
	    return
	  endif

          call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1          istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2          g, ng, jroot)

	  if(flag_lsodar_aborted.eq.1) then
	    return
	  endif

	  tf = t + 100000.

          call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1          istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2          g, ng, jroot)

	  if(flag_lsodar_aborted.eq.1) then
	    return
	  endif

	  tf = t + 0.0001

          call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1          istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2          g, ng, jroot)


	  tf = t + 100000.

	  if(flag_lsodar_aborted.eq.1) then
	    return
	  endif

          call lsodar (derivs, nn, y, t, tf, itol, rtol, atol, itask,
     1          istate, iopt, rwork, lrw, iwork, liw, jac, jt,
     2          g, ng, jroot)

	  if(flag_lsodar_aborted.eq.1) then
	    return
	  endif
	istate = 1
100	continue
101

c	Calculo dos elemtos da matris

c	Jacobiana obtida no fluxo
	J(1, 1) = y(5)
	J(1, 2) = y(6)
	J(1, 3) = y(7)
	J(1, 4) = y(8)

	J(2, 1) = y(9)
	J(2, 2) = y(10)
	J(2, 3) = y(11)
	J(2, 4) = y(12)

	J(3, 1) = y(13)
	J(3, 2) = y(14)
	J(3, 3) = y(15)
	J(3, 4) = y(16)

	J(4, 1) = y(17)
	J(4, 2) = y(18)
	J(4, 3) = y(19)
	J(4, 4) = y(20)

c	Calculo da matris 'B'

	B(1, 1) = J(1, 1) - 1.
	B(1, 2) = J(1, 2)
	B(1, 3) = J(1, 4)
	B(1, 4) = y(1) + 2.*y(1)*(y(2)*y(3))*dsin(y(4))

	B(2, 1) = J(2, 1)
	B(2, 2) = J(2, 2) - 1.
	B(2, 3) = J(2, 4)
	B(2, 4) = -Gamma2*y(2) - (y(1)*y(1))*y(3)*dsin(y(4))

	B(3, 1) = J(3, 1)
	B(3, 2) = J(3, 2)
	B(3, 3) = J(3, 4)
	B(3, 4) = -Gamma3*y(3) - (y(1)*y(1))*y(2)*dsin(y(4))

	B(4, 1) = J(4, 1)
	B(4, 2) = J(4, 2)
	B(4, 3) = J(4, 4) - 1.
	B(4, 4) = -2.*delta + (y(2)**2 + y(3)**2 - 2*y(1)**2)+
     *          (4.*y(2)*y(3) - y(1)**2 * ( y(3)/y(2) + y(2)/y(3)
     *                                    ))*dcos(y(4))


c	Variavel auxiliar para a diferenza entre os valores
c	anteriores, e os novos
	do 200 i = 1, N
	  aux_dif_y(i) = y0(i) - y(i)
200 	continue
	aux_dif_y(3) = 0.

c	write(*, *) aux_dif_y

c	Calculo de la solucion
	lda = N
	job = 0

	call dgefa(B, lda, N, ipvt, info_solver)
	call dgesl(B, lda, N, ipvt, aux_dif_y, job)

	y(1) = y0(1) + aux_dif_y(1)
	y(2) = y0(2) + aux_dif_y(2)
	y(3) = y0(3)
	y(4) = y0(4) + aux_dif_y(3)

c	write(*, *) 'dif: ', aux_dif_y(1), aux_dif_y(2), 0, aux_dif_y(3)


c	Calculo dos autovalores
	call dgeev(jobvl, jobvr, N, J, lda, autov_real, autov_im, vl, ldvl
     *, vr, ldvr, work, lwork, info_eigen)

	do 500 i = 1, N
	  y0(i) = y(i)
500	continue


	return
	end

c Chaotic (strange) and periodic behavior in instability
c saturation by the oscillating two-stream instability
c  D. A. Russell and E. Ott,
c  Physics of Fluids 24(11), pp. 1976-1988

        subroutine derivs(n, t, y, yprime)
        implicit none
        integer n
        double precision y(n), yprime(n), t
        double precision Gamma2, Gamma3, Delta
        double precision df1_dy1, df1_dy2, df1_dy3, df1_dy4
        double precision df2_dy1, df2_dy2, df2_dy3, df2_dy4
        double precision df3_dy1, df3_dy2, df3_dy3, df3_dy4
        double precision df4_dy1, df4_dy2, df4_dy3, df4_dy4
        common/cte/ Gamma2, Gamma3, Delta

        yprime(1) = y(1) + 2.*y(1)*(y(2)*y(3))*dsin(y(4))
        
        yprime(2) = -Gamma2*y(2) - (y(1)*y(1))*y(3)*dsin(y(4))

        yprime(3) = -Gamma3*y(3) - (y(1)*y(1))*y(2)*dsin(y(4))

        yprime(4) = -2.*delta + (y(2)**2 + y(3)**2 - 2*y(1)**2)+
     *          (4.*y(2)*y(3) - y(1)**2 * ( y(3)/y(2) + y(2)/y(3)
     *                                    ))*dcos(y(4))

        df1_dy1 = 1 + 2*y(2)*y(3)*dsin(y(4))
        df1_dy2 = 2*y(1)*y(3)*dsin(y(4))
        df1_dy3 = 2*y(1)*y(2)*dsin(y(4))
        df1_dy4 = 2*y(1)*y(2)*y(3)*dcos(y(4))

        df2_dy1 = -2*y(1)*y(3)*dsin(y(4))
        df2_dy2 = -Gamma2
        df2_dy3 = -y(1)**2 * dsin(y(4))
        df2_dy4 = -y(1)**2 * y(3)*dcos(y(4))

        df3_dy1 = -2*y(1)*y(2)*dsin(y(4))
        df3_dy2 = -y(1)**2 * dsin(y(4))
        df3_dy3 = -Gamma3
        df3_dy4 = -y(1)**2 * y(2)*dcos(y(4))

        df4_dy1 = -4*y(1) - 2*y(1)*(y(3)/y(2) + y(2)/y(3))*dcos(y(4))
     
        df4_dy2 = 2*y(2) + 4*y(3)*dcos(y(4)) 
     *                   + y(1)**2 * y(3)*dcos(y(4))/(y(2)**2)
     *                   - y(1)**2 * dcos(y(4))/y(3)

        df4_dy3 = 2*y(3) + 4*y(2)*dcos(y(4)) - y(1)**2 *dcos(y(4))/y(2)
     *                   + y(1)**2 *y(2)*dcos(y(4))/(y(3)**2)
        
        df4_dy4 = -(4*y(2)*y(3) - y(1)**2 * (y(3)/y(2) 
     *                                      + y(2)/y(3)))*dsin(y(4))
        
c       df1/dy(1) 
        yprime(5) = df1_dy1*y(5) + df1_dy2*y(9)
     *             + df1_dy3*y(13) + df1_dy4*y(17)
        
c       df1/dy(2) 
        yprime(6) = df1_dy1*y(6) + df1_dy2*y(10)
     *             + df1_dy3*y(14) + df1_dy4*y(18)

c       df1/dy(3) 
        yprime(7) = df1_dy1*y(7) + df1_dy2*y(11)
     *             + df1_dy3*y(15) + df1_dy4*y(19)

c       df1/dy(4)
        yprime(8) = df1_dy1*y(8) + df1_dy2*y(12)
     *             + df1_dy3*y(16) + df1_dy4*y(20)

c       df2/dy(1)
        yprime(9) = df2_dy1*y(5) + df2_dy2*y(9)
     *             + df2_dy3*y(13) + df2_dy4*y(17)

c       df2/dy(2)
        yprime(10) = df2_dy1*y(6) + df2_dy2*y(10)
     *             + df2_dy3*y(14) + df2_dy4*y(18)

c       df2/dy(3)
        yprime(11) = df2_dy1*y(7) + df2_dy2*y(11)
     *             + df2_dy3*y(15) + df2_dy4*y(19)

c       df2/dy(4)
        yprime(12) = df2_dy1*y(8) + df2_dy2*y(12)
     *             + df2_dy3*y(16) + df2_dy4*y(20)
c       df3/dy(1)
        yprime(13) = df3_dy1*y(5) + df3_dy2*y(9)
     *             + df3_dy3*y(13) + df3_dy4*y(17)
     
c       df3/dy(2)
        yprime(14) = df3_dy1*y(6) + df3_dy2*y(10)
     *             + df3_dy3*y(14) + df3_dy4*y(18)
     
c       df3/dy(3)
        yprime(15) = df3_dy1*y(7) + df3_dy2*y(11)
     *             + df3_dy3*y(15) + df3_dy4*y(19)
     
c       df3/dy(4)
        yprime(16) = df3_dy1*y(8) + df3_dy2*y(12)
     *             + df3_dy3*y(16) + df3_dy4*y(20)
     
c       df4/dy(1)
        yprime(17) = df4_dy1*y(5) + df4_dy2*y(9)
     *             + df4_dy3*y(13) + df4_dy4*y(17)

c       df4/dy(2)
        yprime(18) = df4_dy1*y(6) + df4_dy2*y(10)
     *             + df4_dy3*y(14) + df4_dy4*y(18)

c       df4/dy(3)
        yprime(19) = df4_dy1*y(7) + df4_dy2*y(11)
     *             + df4_dy3*y(15) + df4_dy4*y(19)

c       df4/dy(4)
        yprime(20) = df4_dy1*y(8) + df4_dy2*y(12)
     *             + df4_dy3*y(16) + df4_dy4*y(20)

        return
        end

      subroutine g (n, t, y, ng, gout)
      double precision t,y(n),gout(ng)

          gout(1) = y(3)-1.

      return
      end

